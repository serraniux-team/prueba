
/*Confif */
// Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
  
import { getDatabase,onValue,ref,set,get,child,update,remove } from
"https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";
import { getStorage, ref as refS, uploadBytes, getDownloadURL} from
"https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  const firebaseConfig = {
    apiKey: "AIzaSyAdSdj6Et-MvxTHQQtM8Ac2W6jWAVSprP4",
    authDomain: "fireserrano-832f7.firebaseapp.com",
    projectId: "fireserrano-832f7",
    databaseURL:"https://fireserrano-832f7-default-rtdb.firebaseio.com/",
    storageBucket: "fireserrano-832f7.appspot.com",
    messagingSenderId: "160793656459",
    appId: "1:160793656459:web:11308caac1ee2f7b27288e",
    measurementId: "G-RQN69192B5"
  };

  // Initialize Firebase
    const app= initializeApp(firebaseConfig);
    const db = getDatabase();
    
    
    var btnInsertar = document.getElementById("btnInsertar");
    var btnBuscar = document.getElementById("btnBuscar");
    var btnActualizar = document.getElementById("btnActualizar");
    var btnBorrar = document.getElementById("btnBorrar");
    var btnTodos = document.getElementById("btnTodos");
    var lista = document.getElementById("lista");
    var btnLimpiar = document.getElementById('btnLimpiar');
    var archivo=document.getElementById("archivo")
    

// Insertar
    var idproducto = "";
    var nombre = "";
    var descripcion = "";
    var precio = "";
    var status="";
    var url="";
    var nameI="";

    function leerInputs(){
        idproducto=document.getElementById("idp").value;
        nombre=document.getElementById("nombre").value;
        descripcion=document.getElementById("Descripcion").value;
        precio=document.getElementById("Precio").value;
        status=document.getElementById("Status").value;
        nameI =document.getElementById("nameI").value;
        url=document.getElementById("url").value;
        

    }
    function insertDatos(){
        leerInputs();
        if ( idproducto==""|| nombre=="" || precio=="" || url=="") {
            alert("Falta lgun dato, revisa nuevamente");
        }
        else{
            set(ref(db,'productos/' + idproducto),{
                nombre: nombre,
                descripcion:descripcion,
                precio:precio,
                status:status,
                nameI:nameI,
                url:url})


            .then((docRef) => {
            alert("Registro exitoso");
            mostrarProductos();
            limpiar();
            })
            .catch((error) => {
            alert("Error en el registro")
            });
        }

    };
// mostrar datos
    function mostrarProductos(){
        const db = getDatabase();
        const dbRef = ref(db, 'productos');
        onValue(dbRef, (snapshot) => {
        lista.innerHTML=""
        snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const childData = childSnapshot.val();

        lista.innerHTML = "<div> " + lista.innerHTML +"Id Producto: "+ childKey +
        " Nombre:  "+childData.nombre +"Descripccion: "+ childData.descripcion +" Precio: "+childData.precio +"Status :"+childData.status+ "<br>"+" </div>";
        
        // ...
        });
        }, {
        onlyOnce: true
        });
    }
    function actualizar(){
        leerInputs();
        if ( idproducto==""|| nombre=="" || precio=="" || url=="") {
            alert("Falta lgun dato, revisa nuevamente");
        }
        else{
            update(ref(db,'productos/' + idproducto),{
                nombre: nombre,
                descripcion:descripcion,
                precio:precio,
                status:status,
                nameI:nameI,
                url:url
            }).then(()=>{
            alert("se realizo actualizacion");
            mostrarProductos();
            limpiar();
            })
            .catch(()=>{
            alert("Surgio un error " + error );
            });
        }
       
    }
    function escribirInpust(){
        document.getElementById("idp").value=idproducto;
        document.getElementById("nombre").value=nombre;
        document.getElementById("Descripcion").value=descripcion;
        document.getElementById("Precio").value=precio;
        document.getElementById("Status").value=status;
        document.getElementById("url").value=url;
        document.getElementById("nameI").value=nameI;
    }
    function borrar(){
        leerInputs();
        if ( idproducto=="") {
            alert("Falta lgun dato, revisa nuevamente");
        }
        else{
            update(ref(db, 'productos/' + idproducto),{
            status:"1"
            }).then(()=>{
                alert("Se deshabilitó");
                
                limpiar();
            }).catch(() => {
                alert("Surgio un error" + error);
            });
        }
        }
        
    function mostrarDatos(){
        leerInputs();
        
        const dbref = ref(db);
        get(child(dbref,'productos/'+ idproducto)).then((snapshot)=>{
        if(snapshot.exists()){
            nombre = snapshot.val().nombre;
            descripcion = snapshot.val().descripcion;
            precio = snapshot.val().precio;
            status=snapshot.val().status;
            nameI=snapshot.val().nameI;
            url=snapshot.val().url;
            
            escribirInpust();
        }
        else {

            alert("No existe");
        }
        }).catch((error)=>{
            alert("error buscar" + error);
        });
    }
    function limpiar(){
        lista.innerHTML="";
        idproducto="";
        nombre="";
        descripcion="";
        precio="";
        status=1;
        nameI="";
        url="";
        escribirInpust();
    }
    btnInsertar.addEventListener('click',insertDatos);
    btnBuscar.addEventListener('click',mostrarDatos);
    btnActualizar.addEventListener('click',actualizar);
    btnBorrar.addEventListener('click',borrar);
    btnTodos.addEventListener('click', mostrarProductos);
    btnLimpiar.addEventListener('click', limpiar);
    archivo.addEventListener('change',cargarImagen);
   
async function cargarImagen(){
    const file= event.target.files[0];
    const name= event.target.files[0].name;
  
    const storage= getStorage();
    const storageRef= refS(storage, 'imagenes/' + name);
  
    await uploadBytes(storageRef, file).then((snapshot) => {
     descargarImagen(); 
      document.getElementById('nameI').value=name;
        
      alert('se cargo la imagen');
    });
  }
  
  async function descargarImagen(){
    archivo= document.getElementById('nameI').value;
    // Create a reference to the file we want to download
  const storage = getStorage();
  const starsRef = refS(storage, 'imagenes/' + archivo);
  
  // Get the download URL
  getDownloadURL(starsRef)
    .then((url) => {
     document.getElementById('url').value=url;
     document.getElementById('imagen').src=url;
    })
    .catch((error) => {
      // A full list of error codes is available at
      // https://firebase.google.com/docs/storage/web/handle-errors
      switch (error.code) {
        case 'storage/object-not-found':
          alert("No existe el archivo");
          break;
        case 'storage/unauthorized':
          alert("No tiene permisos");
          break;
        case 'storage/canceled':
          alert("No existe conexion con la base de datos")
          break;
  
        // ...
  
        case 'storage/unknown':
          alert("Ocurrio algo inesperado")
          break;
      }
    });
  }

  /* Sitio principal */

